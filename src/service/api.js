import axios from 'axios';

const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': 'http://localhost:8080/*'
};

const auth = {
    username: 'pedro',
    password: 'Dev@123'
}

const api = axios.create(
    { baseURL: process.env.REACT_APP_SERVICE_URL }, 
    headers,
    auth
);
  
export default api;
  